@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit User</div>
                <div class="card-body">
                    <?php $current_uri = request()->segments(2); ?>
                    <form>
                        @csrf
                        @method('put')
                        @include('user.fields')
                    </form>
                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <input type="button" id="submit" value="Update" class="btn btn-primary">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script>
    var id = {{ Request::segment(2) }}
    $("#submit").on('click', () => {
    
        var url = '{{ route("user.update", ":id") }}';
        url = url.replace(':id',id);
        var data = $('form').serialize();
        $.post(url, data, function (response) {
            if(response) {
                console.log(response);
                window.location.href = "{{ route('user.index') }}";
            }
        });
    })
</script>
@endsection