@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Users</div>
                <div class="card-body">
                    <a href="{{ route('user.create') }}" class="btn btn-primary">Add New User</a>
                    <table class="table">
                        <thead>
                            <tr>
                                <td>#</td>
                                <td>Name</td>
                                <td>Email</td>
                                <td>Action</td>
                            </tr>
                        </thead>
                        <tbody id="users"></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <br />
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">User Infoes</div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <td>#</td>
                                <td>Full Name</td>
                                <td>IC Number</td>
                                <td>Gender</td>
                                <td>State</td>
                            </tr>
                        </thead>
                        <tbody id="userinfo"></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var url = "{{ route('api.get.users') }}";
    var userInfoUrl = "{{ route('api.get.userinfoes') }}";
    var deleteBtn = function(deleteUrl) {
        return '<form style="display: inline" action=' + deleteUrl + ' method="POST">{{ csrf_field() }} {{ method_field("DELETE") }}<button class="btn fa fa-trash"></button></form>';
    }
    $(() => {
        
        $.get(url, (users) => {
            var i = 1;
            $.each(users, (key, value) => {
                let tableTemplate = function(index, value) {
                    index++;
                    var showUri = `/user/${value.id}`;
                    var editUri = `/user/${value.id}/edit`;
                    return `
                        <tr>
                            <td>${index}</td>
                            <td><a href="${showUri}">${value.name}</a></td>    
                            <td>${value.email}</td>     
                            <td>
                                <a href="${editUri}" class="btn fa fa-edit"></a>
                                ${deleteBtn(showUri)}
                            </td>    
                        </tr>
                    `;
                }

                $('#users').append(tableTemplate(key, value))
            })
        });

        $.get(userInfoUrl, (userInfoes) => {
            var i = 1;
            $.each(userInfoes, (key, value) => {
                console.log(value)
                let tableTemplate = function(index, value) {
                    index++;
                    var showUri = `/userinfo/${value.id}`;
                    var editUri = `/userinfo/${value.id}/edit`;
                    return `
                        <tr>
                            <td>${index}</td>
                            <td>${value.full_name}</td>
                            <td>${value.ic_no}</td>
                            <td>${value.gender}</td>
                            <td>${value.state_id}</td>   
                        </tr>
                    `;
                }
                $('#userinfo').append(tableTemplate(key, value));
            })
        });
    });
</script>
@endsection