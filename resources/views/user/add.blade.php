<div class="form-group row">
    <label for="name" class="col-md-4 col-form-label text-md-right">Name</label>
    <div class="col-md-6">
        <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus autocomplete="off">
    </div>
</div>

<div class="form-group row">
    <label for="email" class="col-md-4 col-form-label text-md-right">Email</label>
    <div class="col-md-6">
        <input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}" required autofocus autocomplete="off">
    </div>
</div>

<div class="form-group row">
    <label for="password" class="col-md-4 col-form-label text-md-right">Password</label>
    <div class="col-md-6">
        <input id="password"  type="password" class="form-control" name="password" value="{{ old('password') }}" required autofocus autocomplete="off">
    </div>
</div>

<div class="form-group row">
    <label for="full_name" class="col-md-4 col-form-label text-md-right">Fullname</label>
    <div class="col-md-6">
        <input id="full_name" type="text" class="form-control" name="full_name" value="{{ old('full_name') }}" required autofocus autocomplete="off">
    </div>
</div>

<div class="form-group row">
    <label for="gender" class="col-md-4 col-form-label text-md-right">Gender</label>
    <div class="col-md-6">
        <select id="gender" name="gender" class="form-control" required>
            <option selected disabled></option>
                <option value="Male" selected>Male</option>
                <option value="Female" selected>Female</option>
            </select>
    </div>
</div>

<div class="form-group row">
    <label for="ic_no" class="col-md-4 col-form-label text-md-right">IC No.</label>
    <div class="col-md-6">
        <input id="ic_no"  type="number" class="form-control" name="ic_no" value="{{ old('ic_no') }}" required autofocus autocomplete="off">
    </div>
</div>

<div class="form-group row">
    <label for="street" class="col-md-4 col-form-label text-md-right">Street</label>
    <div class="col-md-6">
        <input id="street" type="text" class="form-control" name="street" value="{{ old('street') }}" required autofocus autocomplete="off">
    </div>
</div>

<div class="form-group row">
    <label for="city" class="col-md-4 col-form-label text-md-right">City</label>
    <div class="col-md-6">
        <input id="city" type="text" class="form-control" name="city" value="{{ old('city') }}" required autofocus autocomplete="off">
    </div>
</div>

<div class="form-group row">
    <label for="postcode" class="col-md-4 col-form-label text-md-right">Postcode</label>
    <div class="col-md-6">
        <input id="postcode" type="number" class="form-control" name="postcode" value="{{ old('postcode') }}" required autofocus autocomplete="off">
    </div>
</div>

<div class="form-group row">
    <label for="state_id" class="col-md-4 col-form-label text-md-right">State</label>
    <div class="col-md-6">
        <select id="state_id" name="state_id" class="form-control" required>
            <option selected disabled></option>
            
            @foreach($states as $state)
            <option value="{{ $state->name }}">{{ $state->name }}</option>
            @endforeach
        </select>
    </div>
</div>

<div class="form-group row">
    <label for="country_id" class="col-md-4 col-form-label text-md-right">Country</label>
    <div class="col-md-6">
        <select id="country_id" name="country_id" class="form-control" required>
            <option selected disabled></option>
            
            @foreach($countries as $country)
            <option value="{{ $country->name }}">{{ $country->name }}</option>
            @endforeach
        </select>
    </div>
</div>