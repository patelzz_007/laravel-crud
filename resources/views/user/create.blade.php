@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Add a New User</div>
                <div class="card-body">
                    <form action="{{ route('user.store') }} method='POST">
                    @csrf
                    @include('user.add')
                    </form>
                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <input type="submit" id="submit" value="Save" class="btn btn-primary">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script>
    $("#submit").on('click', () => {
        var url = "{{ route('user.store') }}";
        var data = $('form').serialize();
        $.post(url, data, function (response) {
            if(response) {
                window.location.href = "{{ route('user.index') }}";
            }
        });
    })
</script>
@endsection