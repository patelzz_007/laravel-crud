<?php

namespace App\Http\Controllers;

use App\Models\Countries;
use App\Models\Users;
use App\Models\Userinfoes;
use App\Models\States;
use Illuminate\Http\Request;
use DB;

class UserController extends Controller
{
    public function index()
    {
        return view('user.index');
    }

    public function create()
    {
        $states = States::all();
        $countries = Countries::all();
        return view('user.create')
            ->with('states', $states)
            ->with('countries', $countries);
    }

    public function store(Request $request)
    {
        $userData = $request->only(['name', 'email', 'password']);
        $userData['password'] = bcrypt($request->password);

        $userInfoData = $request->except(['name', 'email', 'password']);

        $user = new Users();
        if ($user->addNewUser($userData, $userInfoData)) {
            return 1;
        }
        // $input = $request->all();
        // dd($input);
        // DB::beginTransaction();
        // try{
        //     Users::create($input);
        // }catch(Exception $e){
        //     DB::rollback();
        //     throw $e;
        // }
    }

    public function show($id)
    {
        $data = Users::find($id)->with('userInfo')->first();
        $data['states'] = States::all();
        $data['countries'] = Countries::all();
 
        return view('user.show')
            ->with('data', (object) $data);
    }

    public function edit($id)
    {
        $data = Users::find($id);
        // dd($data);
        $data['states'] = States::all();
        $data['countries'] = Countries::all();
 
        return view('user.edit')
            ->with('data', (object) $data);
    }

    public function update(Request $request, $id)
    {
        $userId = Users::find($id);
        $userInfoes = Userinfoes::where('user_id', $userId->id)->first();
        $input = $request->all();
        
        try{
            
            $userId->update($input);
            $userInfoes->update($input);
        }catch(Exception $e){
            throw $e;
        }
        return redirect()->route('user.index');
    }

    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            if (Userinfoes::where('user_id', $id)->delete()) {
                if (Users::destroy($id)) {
                    DB::commit();

                    return redirect()->route('user.index');
                }
            }
        } catch (\Throwable $th) {
            DB::rollBack();
            throw $th;
        }

        return redirect()->route('user.index');
    }

    public function getUsers()
    {
        return Users::all();
    }

    public function getUserInfoes()
    {
        return Userinfoes::all();
    }
}
