<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Userinfoes extends Model
{
    public $table = "user_infoes";

    public $fillable = [
        'user_id',
        'full_name',
        'gender',
        'ic_no',
        'street',
        'city',
        'postcode',
        'state_id',
        'country_id',
    ];

}
