<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Users extends Model
{
    public $fillable = [
        'name',
        'email',
        'password'
    ];

    public function addNewUser($userData, $userInfoData)
    {
        DB::beginTransaction();
        try {
            $user = Users::create($userData);
            if ($user) {
                $userInfoData['user_id'] = $user->id; 
                if (Userinfoes::create($userInfoData)) {
                    DB::commit();

                    return 1;
                }
            }
        } catch (\Throwable $th) {
            DB::rollBack();

            throw $th;
        }
    }

    public function userInfo()
    {
        return $this->hasOne(Userinfoes::class, 'user_id');
    }
}
