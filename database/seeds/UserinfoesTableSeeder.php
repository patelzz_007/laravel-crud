<?php

use Illuminate\Database\Seeder;

class UserinfoesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_infoes')->insert([
            'user_id' => '1',
            'full_name' => 'Admin Supervisor',
            'gender' => 'Male',
            'ic_no' => '951203566987',
            'street' => 'Street 1',
            'city' => 'Serdang',
            'postcode' => '12345',
            'state_id' => '1',
            'country_id' => '135',
        ]);
    }
}
