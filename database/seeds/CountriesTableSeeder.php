<?php

use GuzzleHttp\Client;
use Illuminate\Database\Seeder;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $client = new Client();
        $result = $client->get('http://countryapi.gear.host/v1/Country/getCountries');
        $response = json_decode($result->getBody()->getContents());
        $i = 1;
        $countryList = [];
        foreach ($response->Response as $country) {
            array_push($countryList, ['name' => $country->Name]);
        }

        DB::table('countries')->insert($countryList);
    }
}
